﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RebootServers
{
    public class Metaframe
    {
        public int RebootTimes = 0;

        // ip to metaframe name
        public Dictionary<string, string> MetaInfo = new Dictionary<string, string>();

        // metaframe name to reboot param, one to one
        public Dictionary<string, string> rebootParam = new Dictionary<string, string>();

        //ip list
        public SortedSet<string> IPset = new SortedSet<string>();

        //local used metaframe name
        public Dictionary<string, string> metaframeNameID = new Dictionary<string, string>();

        //metaframe name list for powershell script
        public string metaframeNamesString = "";

        public string MetaframeType = "";
        public string RebootStartMsg = "";
        public string CheckStartMsg = "";
        public string PingSuccessfullyMsg = "";
        public string PowershellScriptFile = "";
        public Metaframe(string typeName)
        {
            MetaframeType = typeName;
            RebootStartMsg = "Reboot " + MetaframeType + " metaframes";
            CheckStartMsg = "Check " + MetaframeType + " metaframes";

            PingSuccessfullyMsg = "All " + MetaframeType + " metaframes ping successfully";
            PowershellScriptFile = "lastRebootTime" + ".ps1";

            PrepareMetaframeInfo(typeName);

        }

        private void PrepareMetaframeInfo(string typeName)
        {
            if (typeName == "test")
            {
                //MetaInfo.Add("10.10.210.17", "UATM17");
                MetaInfo.Add("123.11.9.105", "m25");
                MetaInfo.Add("123.11.9.25", "m25");
                MetaInfo.Add("123.11.9.205", "m25");
            }
            else
            {
                string getMeta = @"select *
                                   from MetaframeNameAndIP
                                   where [type] ='"+ typeName +"'";

                DataTable dt = new DataTable();
                using (SqlConnection conn = new SqlConnection(StaticConstant.netcrmDBconn))
                {
                    conn.Open();
                    using(SqlCommand cmd = new SqlCommand(getMeta,conn))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);                      
                    }

                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    string id = dataRow["id"].ToString().Trim();
                    string ip = dataRow["IP"].ToString().Trim();
                    string metaframe = dataRow["Metaframe"].ToString().ToLower().Trim();
                    MetaInfo.Add(ip, metaframe);

                    if(!metaframeNameID.ContainsKey(metaframe))
                    {
                        metaframeNameID.Add(metaframe, id);
                        metaframeNamesString += metaframe+",";
                    }
                }

                metaframeNamesString = metaframeNamesString.Remove(metaframeNamesString.Length - 1, 1);
            }

            setReootParam();
            setpingParam();
        }

        private  void setReootParam()
        {
            // one to one
            foreach (var m in metaframeNameID.Keys)
            {
                String param = @" /r /m \\"+m+@" /c ""Scheduled Reboot"" /f /t 1 /d p:4:1";
                rebootParam.Add(m,param);
            }

        }
        private  void setpingParam()
        {
            foreach (string m in MetaInfo.Keys)
            {
                String param = m;
                IPset.Add(param);
            }
        }

    }
}
