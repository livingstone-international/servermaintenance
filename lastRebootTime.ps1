$mfs = "string_of_metaframes"
$m = $mfs.Split(',')

Get-WmiObject Win32_OperatingSystem -ComputerName $m | select csname, @{LABEL='LastBottUpTime' ; EXPRESSION={$_.ConverttoDateTime($_.lastbootuptime).ToString("u")}}
