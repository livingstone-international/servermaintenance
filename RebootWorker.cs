﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Collections.ObjectModel;
using System.IO;

namespace RebootServers
{
    public class RebootWorker
    {
        private static string connectionStr = StaticConstant.netcrmDBconn;
        private bool RWrebootNow = false;
        private bool MonitorMode = false;

        public Metaframe mfs;
        public int rebootTimesLimit = 5;

        #region public method used for checking and rebooting
        public RebootWorker(string typeName,bool rebootNow, bool monitorMode)
        {
            RWrebootNow = rebootNow;
            MonitorMode = monitorMode;
            mfs = new Metaframe(typeName);
        }
        public void RebootAllMetaframes()
        {
            logInDB("0.0.0.0", mfs.MetaframeType, mfs.RebootStartMsg);
            RebootMetaframes(mfs.rebootParam);
        }

        public void CheckAllMetaframesStatus()
        {
            logInDB("0.0.0.0", mfs.MetaframeType, mfs.CheckStartMsg);
            CheckMetaframesStatus(mfs.IPset);
        }



        public bool HasRebootedlMetaInLast(int minutes)
        {
            string rebootMsg = mfs.RebootStartMsg;
            string commandtext = @" Select *
                                    from MetaframeRebootErrorLog
                                    where ts >  DATEADD(minute, -@minutes, GETDATE())  and Cast(msg as nvarchar(max)) = '" + rebootMsg + "' ";

            SqlParameter m = new SqlParameter("@minutes", SqlDbType.Int);
            m.Value = minutes;
            DataTable dt;
            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                using (SqlCommand cmd = new SqlCommand(commandtext, conn))
                {
                    conn.Open();

                    cmd.Parameters.AddRange(new SqlParameter[] { m });
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                }
            }
            return dt.Rows.Count > 0;
        }


        #endregion


        #region private assist methods
        private void RebootMetaframes(Dictionary<string, string> LocRebootParam)
        {
            mfs.RebootTimes++;

            logInDB("0.0.0.0", "Reboot", "Reboot order : " + mfs.RebootTimes);
            if (LocRebootParam.Count > 0)
            {
                Process p = new Process();
                foreach (KeyValuePair<string, string> list in LocRebootParam)
                {
                    string metaArg = list.Value;
                    ProcessStartInfo s1 = new ProcessStartInfo()
                    {
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                        FileName = "shutdown",
                        Arguments = metaArg
                    };
                    p.StartInfo = s1;
                    p.Start();
                    // Do not wait for the child process to exit before
                    // reading to the end of its redirected stream.
                    // p.WaitForExit();
                    // Read the output stream first and then wait.

                    //string output = "";
                    //Action a = () => { output = p.StandardOutput.ReadToEnd(); };

                    //a.BeginInvoke(null, "");

                    logInDB("1.1.1.1", list.Key, "Reboot Start");
                    p.WaitForExit(120000);
                }
            }
            Thread.Sleep(180000);
        }
        private void CheckMetaframesStatus(SortedSet<string> ipList)
        {
            Dictionary<string, string> RebootdownMetaList = new Dictionary<string, string>();

            // check all metaframe and update reboot list
            Process p = new Process();
            string strRegex = @".*bytes=.*time.*TTL=.*";
            Regex re = new Regex(strRegex);
            // Redirect the output stream of the child process.
            List<Task> tasks = new List<Task>();
            foreach (string ip in ipList)
            {
                {
                    ProcessStartInfo s1 = new ProcessStartInfo()
                    {
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                        FileName = "ping",
                        Arguments = ip
                    };
                    p.StartInfo = s1;
                    p.Start();
                    // Do not wait for the child process to exit before
                    // reading to the end of its redirected stream.
                    // p.WaitForExit();
                    // Read the output stream first and then wait.
                    string output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit(120000);
                    String[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    String result = ip + " " + string.Join("", lines);

                    if (!re.IsMatch(result))// note down no-successful reply
                    {
                        string downMetaframe = mfs.MetaInfo[ip];
                        logInDB(ip, downMetaframe, string.Join("", lines));
                        if (!RebootdownMetaList.ContainsKey(downMetaframe))
                        {
                            RebootdownMetaList.Add(downMetaframe, mfs.rebootParam[downMetaframe]);
                        }
                    }
                    else// noted down the successful reply IP and metaframe name 
                    {
                        string upMetafram = mfs.MetaInfo[ip];
                        logInDB(ip, upMetafram, string.Join("", lines));
                    }
                }
            }

            if (RebootdownMetaList.Count > 0
                && mfs.RebootTimes <= rebootTimesLimit)
            {
                // reboot metaframe, if now we can reboot
                if (RWrebootNow)
                {
                    RebootMetaframes(RebootdownMetaList);

                    //check the metaframes with IP down
                    SortedSet<string> nextCheckmeta = new SortedSet<string>(RebootdownMetaList.Keys);
                    SortedSet<string> nextCheckIp = new SortedSet<string>();

                    //get all the IPs related to the metaframes down
                    foreach (KeyValuePair<string, string> mi in mfs.MetaInfo)
                    {
                        foreach (string m in nextCheckmeta)
                        {
                            if (mi.Value == m)
                            {
                                nextCheckIp.Add(mi.Key);
                            }
                        }
                    }
                    // check metaframe
                    CheckMetaframesStatus(nextCheckIp);
                }
                else
                {
                    string downMeg = PrepareMsgForMetaList(RebootdownMetaList) + GetLastRebootTime() ;                 
                    SendEmail("MetaFrames Down",downMeg);
                }

            }
            else if (RebootdownMetaList.Count > 0
                && mfs.RebootTimes > rebootTimesLimit)
            {
                //report error, no need to wait
                string downMeg = PrepareMsgForMetaList(RebootdownMetaList) + GetLastRebootTime(); ;

                StringBuilder dowmMetaList = new StringBuilder();
                foreach (var item in RebootdownMetaList)
                {
                    dowmMetaList.Append(item.Key+",");
                }
                logInDB("2.2.2.2", mfs.MetaframeType, "MetaFrames Down: " + dowmMetaList.ToString());

                SendEmail("MetaFrames Down", downMeg);
            }
            else if (RebootdownMetaList.Count == 0)
            {
                //report ok

                logInDB("0.0.0.0", mfs.MetaframeType, mfs.PingSuccessfullyMsg);

                if (MonitorMode == false)
                {
                    //wait for few minutes and then run powershell to get last reboot time.Otherwise, some metaframes may be missing
                    Thread.Sleep(180000);
                    string allRebootTime = GetLastRebootTime();
                    SendEmail(mfs.PingSuccessfullyMsg, allRebootTime);
                }

            }
        }

        public string GetLastRebootTime()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + mfs.PowershellScriptFile;
            if (File.Exists(path))
            {
                string psScript_temp = System.IO.File.ReadAllText(path);

                string psScript = psScript_temp.Replace("string_of_metaframes",mfs.metaframeNamesString);

                StringBuilder sb = new StringBuilder();
                sb.Append("\n <br>");
                using (PowerShell ps = PowerShell.Create())
                {
                    ps.AddScript(psScript);
                    Collection<PSObject> psOutput = ps.Invoke();
                    int i = 1 ;
                    foreach (PSObject p in psOutput)
                    {
                        //Console.WriteLine(p.ToString());
                        string[] lines = p.ToString().Split(new char[] { ',', '=', ';', '}' });

                        //Console.WriteLine(lines[1] + "   " + lines[3]);//mfs.metaframeNameID[lines[1].Trim().ToLower()]
                        sb.Append( (i++) + " "+lines[1] + "   " + lines[3] + "\n <br>");
                    }
                }
                return sb.ToString();
            }
            return "";
        }

        #endregion


        #region static methods for class

        public static void logInDB(string ip, string mf, string msg)
        {          
            string commandtext = @"  insert into [MetaframeRebootErrorLog] ( [IP],[Metaframe],[msg] )
                                     values(@ip,@mf,@msg)";

            SqlParameter ipadd = new SqlParameter("@ip", SqlDbType.NVarChar);
            ipadd.Value = ip;
            SqlParameter metaframe = new SqlParameter("@mf", SqlDbType.NVarChar);
            metaframe.Value = mf;
            SqlParameter message = new SqlParameter("@msg", SqlDbType.NVarChar);
            message.Value = msg;

            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                using (SqlCommand cmd = new SqlCommand(commandtext, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddRange(new SqlParameter[] { ipadd, metaframe, message });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        private static string PrepareMsgForMetaList(Dictionary<string, string> RebootdownMetaList)
        {

            String dowMetaMeg = "";
            if (RebootdownMetaList.Count > 0)
            {
                foreach (var item in RebootdownMetaList)
                {
                    dowMetaMeg = item.Key + " is down, please use:    shutdown" + item.Value + "\n <br>";
                }
            }
            return dowMetaMeg;
        }

        public static void SendEmail(String subject, string body)
        {    
            string subjectStr = "Notice: " + new String(subject.Take(50).ToArray());
            string bodyStr = body.Length > 1 ? body : subject;

            try
            {
                string sendFrom = "emailcampaign@livingstone.com.au";
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                //message.From = new MailAddress("email@livingstone.com.au");
                message.From = new MailAddress(sendFrom);
                message.To.Add(new MailAddress("servermaintenance@livingstone.com.au"));
                message.Subject = subjectStr;
                message.Body = bodyStr;
                message.IsBodyHtml = true;
                smtp.Port = 587;
                smtp.Host = "smtp.office365.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new NetworkCredential("email@livingstone.com.au", "Arthur123");
                
                string passencrypted = getMailPasswordFromDB(sendFrom);
                string sendFromPass = DataProtectService.Decrypt(passencrypted, "l0cksm1th");

                smtp.Credentials = new NetworkCredential(sendFrom, sendFromPass);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                smtp.Send(message);
            }
            catch (Exception e)
            {
                logInDB("0.0.0.0", "emailcampaign", e.ToString());
                SendEmailAdmin(subjectStr,bodyStr);
            }
        }
        private static string getMailPasswordFromDB(string email)
        {
            string sql = @"spGetEmailPasswordInSenderAccount '" + email + "'";

            object o;
            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    o = cmd.ExecuteScalar();
                }

            }
            return o.ToString();

        }
        public static void SendEmailAdmin(String subject, string body)
        {

            string dbmail = @"EXEC SendEmailWithRecipientSubjectBody
                                    @subject = '" + subject + @"',
                                    @body = '"+ body + @"' ,
                                    @email = 'servermaintenance@livingstone.com.au'
                               ";//
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionStr))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(dbmail, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                logInDB("0.0.0.0", "administrator", e.ToString());
            }

        }

        //reboot out of working hours
        public static bool checkRebootTime(DateTime chktime)
        {
            DayOfWeek[] rebootDays = { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday };
            bool rebootDay = rebootDays.Contains(chktime.DayOfWeek);

            TimeSpan start = new TimeSpan(08, 0, 0);
            TimeSpan end = new TimeSpan(18, 0, 0);
            bool rebootTime = chktime.TimeOfDay < start || chktime.TimeOfDay > end;

            DayOfWeek[] weekEnd = { DayOfWeek.Saturday, DayOfWeek.Sunday };
            bool isweekEnd = weekEnd.Contains(chktime.DayOfWeek);


            return (rebootDay && rebootTime) || isweekEnd;
        }

        #endregion


    }
}
