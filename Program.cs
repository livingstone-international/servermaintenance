﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;


namespace RebootServers
{
    /*
     * 1 if this program is running in working hours, it will not reboot metaframes.
     *   if it finds metaframes that cannot ping successfully, it will just send email to user.
     *   if all successful, just send email. 
     *   
     *   
     * 2 if it is running after workng hours, and the "reboot all metaframs" not happend in last certain amount of time(12 hours), 
     *   it will reboot all metaframes, and then if it finds metaframes that cannot ping successfully, it will 
     *   reboot the metaframes and check again, up to 5 times, if still has down metaframes, it will send email.
     * 
     * 3 if it is running after workng hours, and the "reboot all metaframs" has happend in last certain amount of time(12 hours), 
     *   it will not reboot , just check and reboot down metaframes. 
     *   
     *   
     * */
    class Program
    {
        //rebootNow is the signal for deciding whether can reboot now. 
        //private static bool rebootNow = false;
        static void Main(string[] args)
        {
            string metaframeType =  ( args == null || args.Length < 1 )? "Normal" : args[0] ;
            
            bool rebootNow = RebootWorker.checkRebootTime(DateTime.Now);

            bool monitor = false;
            RebootWorker rebootWorker = new RebootWorker(metaframeType, rebootNow, monitor);


            // reboot normal metaframes, if time is ok and not rebooted last 12 hours.
            if (rebootNow && !rebootWorker.HasRebootedlMetaInLast(720))
            {
                RebootWorker.SendEmail(rebootWorker.mfs.RebootStartMsg, "v20201210");
                rebootWorker.RebootAllMetaframes();

                // check metaframe       
                RebootWorker.SendEmail(rebootWorker.mfs.CheckStartMsg, "v20201210");
                rebootWorker.CheckAllMetaframesStatus();
            }
            else
            {
                // check metaframe
                RebootWorker.SendEmail(rebootWorker.mfs.CheckStartMsg, "v20201210");         
                rebootWorker.CheckAllMetaframesStatus();
            }


            //Console.ReadKey();
        }

       


    }
}
